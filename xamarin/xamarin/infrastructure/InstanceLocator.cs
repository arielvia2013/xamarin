﻿namespace xamarin.infrastructure
{

    using ViewModels;
    public class InstanceLocator
    {
        #region Propiedades
        public MainViewModel Main

        { 
            get; 
            set; 
        }
        #endregion
        #region constructores
        public InstanceLocator()
        {
            this.Main = new MainViewModel();
        }
        #endregion
    }
}
